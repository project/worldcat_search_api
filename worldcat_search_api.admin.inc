<?php

/**
 * Create the config form that will save the API key to the database.
 */
function worldcat_search_api_configuration_form() {
  // Create the form.
  $form = array();

  // Create a fieldset.
  $form['worldcat_api_settings'] =  array(
    '#type' => 'fieldset',
    '#title' => t('WorldCat API'),
  );

  // Add textfield to the field set.
  $form['worldcat_api_settings']['worldcat_api_key'] = array(
    '#type' => 'textfield',
    '#title' => 'API Key',
    '#default_value' => variable_get('worldcat_api_key', ''),
  );

  // Use system_settings_form() to add a submit handler.
  return system_settings_form($form);
}
